<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>

<style>
#customers {
	HEAD font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#customers td, #customers th {
	border: 1px solid #ddd;
	padding: 8px;
}

#customers tr:nth-child(even) {
	background-color: #f2f2f2;
}

#customers tr:hover {
	background-color: #ddd;
}

#customers th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #6B6565;
	color: white;
}

.container {
	padding: 16px 16px;
}
</style>
</head>
<body>
	<div class="container">

		<c:choose>
			<c:when test="${status_code eq '401' }">
				<p>${status_desc }</p>
			</c:when>

			<c:otherwise>
				<table id="customers">
					<thead>
						<tr>
							<th>Name</th>
							<th>Age</th>
							<th>Active</th>
							<th>Blocked</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${profiles }" var="profile">
							<tr
								onclick="window.location='http://localhost:8080/profile/view?id=${profile.id}';">
								<td>${profile.name.wholeName }</td>
								<td>${profile.age}</td>
								<td>${profile.active eq true ? '&#9745;' :  '&#9744;'} </td>
								<td>${profile.blocked eq true ? '&#9745;' :  '&#9744;'}</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</c:otherwise>
		</c:choose>


	</div>

</body>
</html>