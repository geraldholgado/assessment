package com.assessment.profile.controller;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.assessment.profile.exception.DataNotFoundException;
import com.assessment.profile.manager.ProfileManager;
import com.assessment.profile.model.Profile;

@Controller
public class ViewProfileController {

	public static final String PROFILE_LIST_URI = "http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json";

	@RequestMapping("/")
	public ModelAndView showHomePage() {
		System.out.println("in controller");
		ModelAndView modelAndView = new ModelAndView("index");

		try {
			ArrayList<Profile> profiles = ProfileManager.fetchProfileList(PROFILE_LIST_URI);
			modelAndView.addObject("profiles", profiles);
			modelAndView.addObject("status_desc", "success");
			modelAndView.addObject("status_code", "200");
		} catch (Exception e) {
			System.out.println("Error : " + e);
			modelAndView.addObject("status_desc", "Internal Error");
			modelAndView.addObject("status_code", "401");
		}

		return modelAndView;
	}

	@RequestMapping(value = "/view")
	public ModelAndView viewProfile(HttpServletRequest request) {
		String id = request.getParameter("id");
		System.out.println("id " + id);
		ModelAndView modelAndView = new ModelAndView("profile");
		try {
			Profile profile = ProfileManager.fetchProfile(PROFILE_LIST_URI, new Profile(id));
			modelAndView.addObject("profile", profile);
			modelAndView.addObject("status_desc", "success");
			modelAndView.addObject("status_code", "200");
		} catch (DataNotFoundException e) {
			System.out.println("Error : " + e);
			modelAndView.addObject("status_desc", "No Data Found");
			modelAndView.addObject("status_code", "400");
		} catch (Exception e) {
			System.out.println("Error : " + e);
			modelAndView.addObject("status_desc", "Internal Error");
			modelAndView.addObject("status_code", "401");
		}
		return modelAndView;
	}

}
