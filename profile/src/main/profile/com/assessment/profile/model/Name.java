package com.assessment.profile.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Name {

	public static final Name NONE = new Name("", "", "");

	private final String first;

	private final String middle;

	private final String last;

	public Name(@JsonProperty("first") String first, @JsonProperty("middle") String middle,
			@JsonProperty("last") String last) {
		super();
		this.first = first;
		this.middle = middle;
		this.last = last;
	}

	public String getFirst() {
		return first;
	}

	public String getMiddle() {
		return middle;
	}

	public String getLast() {
		return last;
	}

	@JsonGetter("whole_name")
	public String getWholeName() {
		return first + " " + middle + " " + last;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((last == null) ? 0 : last.hashCode());
		result = prime * result + ((middle == null) ? 0 : middle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Name other = (Name) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (last == null) {
			if (other.last != null)
				return false;
		} else if (!last.equals(other.last))
			return false;
		if (middle == null) {
			if (other.middle != null)
				return false;
		} else if (!middle.equals(other.middle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Name [first=");
		builder.append(first);
		builder.append(", middle=");
		builder.append(middle);
		builder.append(", last=");
		builder.append(last);
		builder.append("]");
		return builder.toString();
	}

}
