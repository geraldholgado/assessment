package com.assessment.profile.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Profile {

	private final String id;

	private final Boolean active;

	private final Boolean blocked;

	private final String balance;

	private final String picture;

	private final Integer age;

	private final Name name;

	private final String email;

	private final String phone;

	private final String address;

	private final String profile;

	private final long dateRegistered;

	public Profile(@JsonProperty("id") String id, @JsonProperty("active") Boolean active,
			@JsonProperty("blocked") Boolean blocked, @JsonProperty("balance") String balance,
			@JsonProperty("picture") String picture, @JsonProperty("age") Integer age, @JsonProperty("name") Name name,
			@JsonProperty("email") String email, @JsonProperty("phone") String phone,
			@JsonProperty("address") String address, @JsonProperty("profile") String profile,
			@JsonProperty("date_registered") Long dateRegistered) {
		super();
		this.id = id;
		this.active = active;
		this.blocked = blocked;
		this.balance = balance;
		this.picture = picture;
		this.age = age;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.profile = profile;
		this.dateRegistered = dateRegistered;
	}

	public Profile(String id) {
		super();
		this.id = id;
		this.active = false;
		this.blocked = false;
		this.balance = "0";
		this.picture = "";
		this.age = 0;
		this.name = Name.NONE;
		this.email = "";
		this.phone = "";
		this.address = "";
		this.profile = "";
		this.dateRegistered = 0L;
	}

	public String getId() {
		return id;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public String getBalance() {
		return balance;
	}

	public String getPicture() {
		return picture;
	}

	public Integer getAge() {
		return age;
	}

	public Name getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public String getProfile() {
		return profile;
	}

	@JsonGetter("date_registered")
	public long getDateRegistered() {
		return dateRegistered;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Profile [id=");
		builder.append(id);
		builder.append(", active=");
		builder.append(active);
		builder.append(", blocked=");
		builder.append(blocked);
		builder.append(", balance=");
		builder.append(balance);
		builder.append(", picture=");
		builder.append(picture);
		builder.append(", age=");
		builder.append(age);
		builder.append(", name=");
		builder.append(name);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", address=");
		builder.append(address);
		builder.append(", profile=");
		builder.append(profile);
		builder.append(", dateRegistered=");
		builder.append(dateRegistered);
		builder.append("]");
		return builder.toString();
	}

}
