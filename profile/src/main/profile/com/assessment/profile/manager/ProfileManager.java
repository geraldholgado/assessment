package com.assessment.profile.manager;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import com.assessment.profile.exception.DataNotFoundException;
import com.assessment.profile.model.Profile;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProfileManager {

	public static ArrayList<Profile> fetchProfileList(String uri)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL(uri);
		ArrayList<Profile> profiles = objectMapper.readValue(url, new TypeReference<ArrayList<Profile>>() {
		});
		return profiles;
	}

	public static Profile fetchProfile(String uri, Profile profile)
			throws JsonParseException, JsonMappingException, IOException, DataNotFoundException  {
		ArrayList<Profile> profiles = fetchProfileList(uri);
		for (Profile p : profiles) {
			if (profile.equals(p))
				return p;
		}
		throw new DataNotFoundException("Data not found.");

	}
}
